package modules

import com.google.inject.AbstractModule
import com.google.inject.Scopes
import service.DataService
import db.DbCommand

/**
 * Created by kp on 10/24/2015.
 */
class CDHackModule extends AbstractModule{
    @Override
    protected void configure() {
        bind(DataService.class).in(Scopes.SINGLETON)
        bind(DbCommand.class).in(Scopes.SINGLETON)
    }
}
