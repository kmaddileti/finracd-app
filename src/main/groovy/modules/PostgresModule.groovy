package modules

import com.google.inject.AbstractModule
import com.google.inject.Scopes
import org.postgresql.ds.PGSimpleDataSource

import javax.sql.DataSource
import javax.inject.Provider

/**
 * Created by kp on 10/24/2015.
 */
class PostgresModule extends AbstractModule {
    @Override
    protected void configure() {
        bind(DataSource.class).toProvider(PostgresDataSourceProvider.class).in(Scopes.SINGLETON)
    }

    static class PostgresDataSourceProvider implements Provider<DataSource> {

        private String user = "postgres"
        private String password = "admin"
        private String hostname = "localhost"
        private String db = "FINRACD"

        public PostgresDataSourceProvider() { }

        @Override
        DataSource get() {
            final DataSource dataSource = new PGSimpleDataSource()
            dataSource.setUser(user)
            dataSource.setPassword(password)
            dataSource.setServerName(hostname)
            dataSource.setDatabaseName(db)

            dataSource

        }
    }
}
