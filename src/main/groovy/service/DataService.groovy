package service

import com.google.inject.Inject
import db.DbCommand
import groovy.json.JsonBuilder

/**
 * Created by kp on 10/24/2015.
 */
class DataService {

    private DbCommand db

    @Inject
    DataService(DbCommand dbCommand) {
        this.db = dbCommand
    }

    def getResults() {
        return new JsonBuilder(db.getResults()).toPrettyString()
    }
}
