package db

import com.google.inject.Inject
import groovy.sql.GroovyRowResult
import groovy.sql.Sql
import ratpack.exec.Blocking

/**
 * Created by kp on 10/24/2015.
 */
class DbCommand {
    private Sql sql

    @Inject
    DbCommand(Sql sql) {
        this.sql = sql
    }

    List<GroovyRowResult> getResults() {
            sql.rows("select * from weather")
    }
}
