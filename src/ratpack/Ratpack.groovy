import modules.CDHackModule
import modules.PostgresModule
import org.reactivestreams.Publisher
import ratpack.groovy.sql.SqlModule
import ratpack.groovy.template.MarkupTemplateModule
import ratpack.sse.ServerSentEvents
import service.DataService

import java.time.Duration

import static ratpack.groovy.Groovy.groovyMarkupTemplate
import static ratpack.groovy.Groovy.ratpack

import static ratpack.sse.ServerSentEvents.serverSentEvents;
import static ratpack.stream.Streams.periodically;

ratpack {
  bindings {
    module MarkupTemplateModule
    module SqlModule
    module PostgresModule
    module CDHackModule
  }

  handlers { DataService dataService ->
    get {
      render groovyMarkupTemplate("index.gtpl", title: "My Ratpack App")
    }
    get("weather") {
      render dataService.getResults()
    }
    get("sse") {
      Publisher<String> stream = periodically(context, Duration.ofMillis(5), { i -> i < 5 ? i.toString() : null });

      ServerSentEvents events = serverSentEvents(stream, { e ->
        e.id({ arg ->  arg.toString() }).event("counter").data({ arg -> "{ \"status\": \"${arg}\" }".toString()  })
      })
      render(events)

    }

    files { dir "public" }
  }
}
