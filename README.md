Data visualization Using dc.js, ratpack, gradle 
================================================
Running the application

a) Assumes localhost running postgres sql 

Table name : metrics


| Column Name   | DataType      | 
| ------------- |:-------------:| 
| testcaseid      | integer | 
| testdescription      | text      | 
| storyid | text      |
| testcategory | text      |
| priority | text      |
| component | text      |
| status | text      |
| testtype | text      |


Run the following command in finracd-app directory

./gradlew run


Contributers to Team DeepFriedDish
===================================

Krishna Maddileti

Jagdeep Saini

Jagadish Pamarthi

Sanjana Seetharam

Link to presentation
====================

https://prezi.com/tswijouohrzg/microservices-/?utm_campaign=share&utm_medium=copy

References
==========
https://becomingadatascientist.wordpress.com/tag/dc-js/

https://dc-js.github.io/dc.js/

https://github.com/ratpack/example-books

http://kyleboon.org/blog/2014/08/14/ratpack-plus-docker-plus-gradle/